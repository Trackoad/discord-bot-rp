"use strict";

const fs = require('fs');
const Config = require('../config/config.json');

const MapRp = {
  data:null,
  pathName_:null
};

MapRp.init = () => {
  let crypto = require('crypto');

  let filename = crypto.createHash('md5').update(Config.saloon.name).digest('hex');

  MapRp.pathName_ = `./data/${filename}.json`;
  try {

    if (fs.existsSync(MapRp.pathName_)) {
      MapRp.data = require("." + MapRp.pathName_);
    } else {
      let Area = require('../entities/area.js');
      let Room = require('../entities/room.js');
      let Item = require('../entities/item.js');
      MapRp.data = require('../entities/MapDefault.json');
      MapRp.data.name = Config.saloon.name;
      MapRp.data.areas.push(new Area("bilbao").addRoom(new Room("Inventaire", "inventory")));
      console.log(MapRp.data);
      this.saveMap();

    }
  } catch(err) {
      console.log(err);
  }
  console.log(MapRp.data);
};

this.saveMap = () => {
  let file = fs.openSync(MapRp.pathName_, "w+");
  fs.writeFile(MapRp.pathName_, JSON.stringify(MapRp.data), (err) => {
    if(err) console.log(err);
  });
  fs.closeSync(file);
};

module.exports = MapRp;
