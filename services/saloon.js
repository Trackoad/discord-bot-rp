"use strict";

const Config = require('../config/config.json');

const Saloon = {

};

Saloon.init = (client) => {
  if(!this.saloonExist(client)){
    client.guilds.first().createChannel(Config.saloon.name, "category");
  }
};



this.saloonExist = (client) => {
  let result = false;
  client.channels.map((element)=>{
    if(this.isAnCategory(element) && this.isConfigName(element)){
      result = true;
      return;
    }
  })

  return result;
};

this.isAnCategory = (cat) => {
  return cat.type === "category";
};

this.isConfigName = (cat) => {
  return Config.saloon.name === cat.name;
};

module.exports = Saloon;
