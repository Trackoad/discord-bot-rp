"use strict";

class ButtonInteract {
  constructor(emote, name){
    this.emote = Button.getEmojiByName(emote);
    this.name = name;
    this.sendInteract = (msg, callback, context) => {
      msg.react(this.emote);
      msg.interaction = {
        name:this.name,
        context:context,
        action:callback
      };
      debugger;
    };

  }

  changeContext(context){
    this.context = context;
    return this;
  }
}

const Button = {Client:null};

Button.init = (client) => {
  Button.Client = client;
  Button.Inventory = new ButtonInteract(":handbag:", "inventory");
  Button.Money = new ButtonInteract(":moneybag:", "openmoney");
  Button.Check = new ButtonInteract(":white_check_mark:", "greergregerg");
  Button.UnCheck = new ButtonInteract(":negative_squared_cross_mark:", "bilou");
};

Button.send = (button, msg, callback, context = null) => {
  button.sendInteract(msg, callback, context);
};

Button.event = (msg, user) => {
  if(user.bot){
    return;
  }
  if(msg.message.interaction !== undefined){
    msg.message.interaction.action(msg,user);
  }
}

Button.getEmojiByName = (name) => {
  let EmojisReal = require('emojis-list');
  let emojis = require('emojis-keywords');
  return EmojisReal[emojis.findIndex( test => test === name)];
};

module.exports = Button;
