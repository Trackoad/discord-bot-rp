"use strict";

const Button = require('./button.js');

const Choices = {
  channels:{
    TEXT_CMD:"550758040706744320"
  }
};

Choices.send = (channel, message, buttons) => {
  channel.send(message).then(evt => {
    buttons.forEach((button) => {
      Button.send(button.name, evt, button.action);
    });
  });
};

Choices.init = (client) => {

  Object.keys(Choices.channels).forEach((name) => {
    Choices.channels[name] = client.channels.get(Choices.channels[name]);
  });
};

Choices.buttonChoice = (buttonType, action) => {
  return {
    name:buttonType,
    action:action
  }
};

module.exports = Choices;
