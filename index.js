const Discord = require('discord.js');

const Config = require('./config/config.json');

const client = new Discord.Client();

require('./Controllers/mainEvents.js').init(client);

client.login(Config.token);
