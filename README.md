# 😜 Role-play project on Discord 💬

##Auteurs
> Trackoad

### Tech

Dillinger uses a number of open source projects to work properly:

* [Discord](https://discordapp.com/) - Discord
* [Node.js] - du js

And of course **This project** is open source with a [https://gitlab.com/Trackoad/discord-bot-rp] on GitLab.

### Installation

```
make
```

### Launch

**Mode Debug**

```
node --inspect-brk index.js
```

**Without Debug**

```
node index.js
```
