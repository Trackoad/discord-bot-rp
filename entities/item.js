"use strict";

module.exports = class Item {
  constructor(name, description, weight){
    this.name = name;
    this.description = description;
    this.weight = weight;
  }
}
