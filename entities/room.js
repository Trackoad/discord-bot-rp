"use strict";

module.exports = class Room {
  constructor(name, interaction){
    this.id = -1;
    this.name = name;
    this.players = [];
    this.interaction = interaction;
    this.objects = [];
  }
}
