"use strict";

module.exports = class Player {
  constructor(tagDiscord){
    this.tagDiscord = tagDiscord;
    this.idRoom = 0;
    this.objects = [];
    this.money = 0;
  }
}
