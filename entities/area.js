"use strict";

module.exports = class Area {
  constructor(name){
    this.name = name;
    this.rooms = [];
  }

  addRoom(room){
    this.rooms.push(room);
    return this;
  }
}
