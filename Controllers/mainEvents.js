"use strict";

const Config = require('../config/config.json');

const Saloon = require('../services/saloon.js');

const Button = require('../services/button.js');

const MapRp = require('../services/map.js');

const Choices = require('../services/choices.js');

const Events = {
  Client:null
};

Events.start = () => {
  console.log(`Logged in as ${Events.Client.user.tag} !`);
  MapRp.init();
  Button.init(Events.Client);
  Saloon.init(Events.Client);
  Choices.init(Events.Client);
};

Events.reactionMessage = (msg, user) => {
  Button.event(msg, user);
};


Events.receiveMessage = (msg) => {
  this.commandEvent(msg);
};

this.commandEvent = (msg) => {
  if(msg.content[0] !== "!") return;
  let args = msg.content.substr(1, msg.content.length - 1).split(" ");

  if(args[0] === undefined || args[0].match(/[^a-z]/)){
    msg.reply("Bad command");
    return;
  }

  let fs = require('fs');
  let path = `commands/${args[0]}.js`;

  if(fs.existsSync("Controllers/" +path)) {
    let Command = require("./" + path);

    let command = new Command(msg);
    if(command.execute !== undefined){
      command.execute(args);
    }



  } else
    msg.reply(`La commande ${args[0]} n'existe pas.`);
};

this.sendInteract = (msg, emote, callback, name, context = null) => {
  msg.react(emote);
  msg.interaction = {
    name:name,
    context:context,
    action:callback
  };
};

Events.init = (client) => {
  client.on('ready', Events.start);
  client.on('message', Events.receiveMessage);
  client.on("messageReactionAdd", Events.reactionMessage);
  Events.Client = client;
};

module.exports = Events;
