"use strict";


module.exports = class PurgeCommand {
  constructor(msg){
    this.message = msg;
  }

  execute(args){
    try{
      this.message.channel.bulkDelete(50);
    } catch(err){
      this.message = undefined;
    }


  }
}
